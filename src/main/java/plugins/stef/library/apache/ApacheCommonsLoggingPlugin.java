package plugins.stef.library.apache;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the Apache Commons Logging library.
 * 
 * @author Stephane Dallongeville
 */
public class ApacheCommonsLoggingPlugin extends Plugin implements PluginLibrary
{
    //
}
